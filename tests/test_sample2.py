'''coding:utf-8'''
# ------------------------------------------------------------
# 処理名  ： pytestサンプル
# 作成者  ： NRI藤田一樹
# 作成日  ： 2020/08/27
# 処理概要： pytest実行用のテストコードサンプル
# ------------------------------------------------------------
# ライブラリのインポート
import pytest

# テスト対象関数のインポート
from src.sample import add
# from src.sample import sub


# ------------------------------------------------------------
# ★★★★★★  テスト実行処理部分  ★★★★★★
# ------------------------------------------------------------
# =====[add関数]=====
# テストケースのセット
add_params = {
    "a:int b:int": (1, 2, 3),
    "a:int b:float": (1, 2.0, 3.0),
    "a:float b:float": (1.0, 2.0, 3.0),
    "a:int(negative) b:float(negative)": (-1, -2.0, -3.0),
}


# テスト実行
@pytest.mark.parametrize(
    "a, b, expected", list(add_params.values()),
    ids=list(add_params.keys()))
def test_add(a, b, expected):
    '''
    足し算結果を検証する
    '''
    assert add(a, b) == expected
