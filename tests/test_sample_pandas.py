'''coding:utf-8'''
# ------------------------------------------------------------
# 処理名  ： pytestサンプル
# 作成者  ： NRI藤田一樹
# 作成日  ： 2020/08/27
# 処理概要： pytest実行用のテストコードサンプル
# ------------------------------------------------------------
# ライブラリのインポート
import pandas as pd
from pandas.testing import assert_frame_equal

# テスト対象関数のインポート
from src.sample_pandas import get_pos_jisseki


# ------------------------------------------------------------
# ★★★★★★  テスト実行処理部分  ★★★★★★
# ------------------------------------------------------------
def test_get_pos_jisseki1():
    '''
    POS実績データをロード処理のテスト実施
    '''

    # 想定結果の作成
    df_expected = pd.DataFrame({
        'kijun_ymd': ['20200820'],
        'jifun': ['0659'],
        'org_mise': ['423574'],
        'pma': ['46'],
        'jo': ['G'],
        'item': ['460064'],
        'suryo': [2]
    })

    # 実行結果比較
    assert_frame_equal(
        get_pos_jisseki(), df_expected, check_dtype=True)


# def test_get_pos_jisseki2():
#     '''
#     POS実績データをロード処理のテスト実施
#     '''

#     # 想定結果の作成
#     df_expected = pd.read_csv(
#         'D:/2020_python_DX研修/2020_python_ex/400_pytest編/pytestサンプル_pandas編//tests/expected/sample_expected.csv',
#         dtype={'kijun_ymd': str, 'jifun': str, 'org_mise': str,
#                'pma': str, 'jo': str, 'item': str})

#     # 実行結果比較
#     assert_frame_equal(
#         get_pos_jisseki(), df_expected, check_dtype=True)
