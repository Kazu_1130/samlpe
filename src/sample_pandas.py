'''coding:utf-8'''
# ------------------------------------------------------------
# 処理名  ： pytestサンプル
# 作成者  ： NRI藤田一樹
# 作成日  ： 2020/08/27
# 処理概要： pytest実行用のロジック処理サンプル
# ------------------------------------------------------------
import pandas as pd


# ------------------------------------------------------------
# ★★★★★★  関数処理部分  ★★★★★★
# ------------------------------------------------------------
def get_pos_jisseki():
    '''
    POS実績データをロードして抽出対象データを出力する

    Parameters
    ----------
    None

    Returns
    ----------
    df_pos_jisseki : DataFrame
        [POS実績データ]
    '''
    # POS実績データのロード
    # df_pos_jisseki = pd.read_csv(
    #     'D:/2020_python_DX研修/2020_python_ex/400_pytest編/pytestサンプル_pandas編/data/sample.csv',
    #     dtype={'kijun_ymd': str, 'jifun': str, 'org_mise': str,
    #            'pma': str, 'jo': str, 'item': str})

    # 想定結果の作成
    df_pos_jisseki = pd.DataFrame({
        'kijun_ymd': ['20200820'],
        'jifun': ['0659'],
        'org_mise': ['423574'],
        'pma': ['46'],
        'jo': ['G'],
        'item': ['460064'],
        'suryo': [2]
    })

    # 横浜野村ビル店のPMA46の販売実績データのみを抽出
    df_pos_jisseki = df_pos_jisseki[
        (df_pos_jisseki['org_mise'] == '423574')
        & (df_pos_jisseki['pma'] == '46')]

    # インデックスをリセットする
    df_pos_jisseki.reset_index(inplace=True, drop=True)

    return df_pos_jisseki
